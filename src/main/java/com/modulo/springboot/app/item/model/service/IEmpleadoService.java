package com.modulo.springboot.app.item.model.service;

import java.util.List;

import com.modulo.springboot.app.item.model.entity.Empleado;



public interface IEmpleadoService {
	
	public Empleado findByUsername(String username);
	
	public Empleado findByEmail(String email);
	
	public List<Empleado> findAll();
	
	public Empleado findById(Long id);
	
	public Empleado save(Empleado empleado);
	
	public void delete(Long id);
}
