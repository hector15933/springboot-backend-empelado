package com.modulo.springboot.app.item.model.service;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.modulo.springboot.app.item.model.dao.IEmpleadoDao;
import com.modulo.springboot.app.item.model.entity.Empleado;


@Service
public class EmpleadoService implements IEmpleadoService{
	

	@Autowired
	private IEmpleadoDao usuarioDao;

	@Override
	public List<Empleado> findAll() {
		// TODO Auto-generated method stub
		return (List<Empleado>) usuarioDao.findAll();
	}
	@Override
	public Empleado findById(Long id) {
		// TODO Auto-generated method stub
		return usuarioDao.findById(id).orElse(null);
	}
	@Override
	public Empleado save(Empleado empleado) {
		// TODO Auto-generated method stub
		return usuarioDao.save(empleado);
	}
	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		usuarioDao.deleteById(id);
	}
	@Override
	@Transactional(readOnly = true)
	public Empleado findByUsername(String username) {
		// TODO Auto-generated method stub
		return usuarioDao.findByUsername(username);
	}
	@Override
	public Empleado findByEmail(String email) {
		// TODO Auto-generated method stub
		return usuarioDao.findByEmail(email);
	}

}
