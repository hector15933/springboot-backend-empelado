package com.modulo.springboot.app.item;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootApp001pApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootApp001pApplication.class, args);
	}

}
