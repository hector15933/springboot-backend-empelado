package com.modulo.springboot.app.item.controller;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.modulo.springboot.app.item.model.entity.Empleado;
import com.modulo.springboot.app.item.model.service.IEmpleadoService;


@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/api")
public class EmpleadosController {
	
	@Autowired
	private IEmpleadoService empleadoService;
	
	
	@GetMapping("/empleados/listar")
	public ResponseEntity<?>  listar() {
		
		Map<String,Object> response = new HashMap<>();
		List<Empleado> listas=null;
		try {
			
			listas = empleadoService.findAll();
			response.put("empleados", listas);

			
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar la consulta en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<Map<String,Object>>( response,HttpStatus.OK);
	}
	
	
	
	@GetMapping("/empleados/{id}")
	public ResponseEntity<?> show(@PathVariable Long id){
		Map<String,Object> response = new HashMap<>();
		Empleado empleado = null;
		try{
			 empleado = empleadoService.findById(id);	
		}catch(DataAccessException e){
			response.put("mensaje", "Error al realizar la consulta en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if(empleado==null) {
			response.put("mensaje", "El Cliente ID: ".concat(id.toString().concat(" no existe en la base de datos!")));
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Empleado>(empleado,HttpStatus.OK);		
	}
	
	@PostMapping("/empleados/crear")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<?> create(@Valid @RequestBody Empleado empleado,BindingResult bindingResult) {
		Map<String,Object> response = new HashMap<>();
		
		if(bindingResult.hasErrors()) {
			
			List<String> errors= new ArrayList<>();
			for(FieldError err : bindingResult.getFieldErrors()){
				errors.add("El campo"+err.getField()+" "+err.getDefaultMessage());
			}
			
					response.put("errors", response);
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.BAD_REQUEST);
		}
		
		Empleado empleadoNew = null;
		try {
			String aux;
			if(empleado.getNacionalidad().equals("Colombia")) {
				aux=".co";
			}else {
				aux=".eu";
			}
			
				empleadoNew= empleadoService.save(empleado);
				//empleadoService.findByEmail(empleadoNew.getPrimerNombre().toLowerCase()+empleadoNew.getPrimerApellido().toLowerCase()+"@"+"cidenet.com"+aux);
				
				String nombre1,apellido1=new String();
				nombre1=empleadoNew.getPrimerNombre().toLowerCase().replaceAll("\\s","");
				apellido1=empleadoNew.getPrimerApellido().toLowerCase().replaceAll("\\s","");
				
				empleadoNew.setEmail(nombre1+apellido1+empleadoNew.getId().toString()+"@"+"cidenet.com"+aux);
				empleadoNew= empleadoService.save(empleadoNew);	
			
				
				
			
		}catch(DataAccessException e){
			
			response.put("message", "Error al realizar insert en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		response.put("message", "Creado correctamente");
		response.put("cliente", empleadoNew);
		
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.CREATED);	
	}
	
	
	@PutMapping("/empleados/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<?> editar(@RequestBody Empleado empleado,@PathVariable Long id){
		Empleado empleadoActual= empleadoService.findById(id);
		Empleado empleadoUpdated = null;

		Map<String,Object> response= new HashMap<>();
		if(empleadoActual == null) {	
			response.put("mensaje","Error: No se puede editar el  ID: ".concat(id.toString().concat("no existe en la base de datos")));
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.NOT_FOUND);
		}
		try {
			String aux;
			if(empleado.getNacionalidad().equals("Colombia")) {
				aux=".co";
			}else {
				aux=".eu";
			}
			
			String nombre1,apellido1=new String();
			nombre1=empleado.getPrimerNombre().toLowerCase().replaceAll("\\s","");
			apellido1=empleado.getPrimerApellido().toLowerCase().replaceAll("\\s","");
			/*empleadoActual.setApellidos(empleado.getApellidos());
			empleadoActual.setNombres(empleado.getNombres());
			empleadoActual.setDni(empleado.getDni());
			empleadoActual.setDireccion(empleado.getDireccion());
			empleadoActual.setEmail(empleado.getEmail());*/
			empleado.setUpdateAt(new Date());
			empleado.setEmail(nombre1+apellido1+empleado.getId().toString()+"@"+"cidenet.com"+aux);
			
			empleadoUpdated= empleadoService.save(empleado);	
		}catch(DataAccessException e) {
			response.put("message", "Error al actualizar datos en la base de datos");
			response.put("error", e.getMostSpecificCause().getMessage());
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);	
		}	
		response.put("message", "Actualizado correctamente");
		response.put("empleado", empleadoUpdated);
		
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.CREATED);	
	}
	
	
	
	@DeleteMapping("/empleados/eliminar/{id}")
	public ResponseEntity<?> eliminar(@PathVariable Long id){
		Map<String,Object> response = new HashMap<>();
		
		try {
			empleadoService.delete(id);
		}catch(DataAccessException e) {
			response.put("message", "Error al eliminar datos en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);	
		}
		
		response.put("message", "Eliminado correctamente");
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.OK);	
	}
	
	@PutMapping("/empleados/eliminar2/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<?> editar2(@PathVariable Long id){
		Empleado empleadoActual= empleadoService.findById(id);
		Empleado empleadoUpdated = null;

		Map<String,Object> response= new HashMap<>();
		if(empleadoActual == null) {	
			response.put("mensaje","Error: No se puede editar el  ID: ".concat(id.toString().concat("no existe en la base de datos")));
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.NOT_FOUND);
		}
		try {
			/*empleadoActual.setApellidos(empleado.getApellidos());
			empleadoActual.setNombres(empleado.getNombres());
			empleadoActual.setDni(empleado.getDni());
			empleadoActual.setDireccion(empleado.getDireccion());
			empleadoActual.setEmail(empleado.getEmail());*/
			//empleadoActual.setEnabled(false);
			empleadoActual.setEnabled(false);
			empleadoUpdated= empleadoService.save(empleadoActual);	
		}catch(DataAccessException e) {
			response.put("message", "Error al actualizar datos en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);	
		}	
		response.put("message", "Eliminado correctamente");
		response.put("empleado", empleadoUpdated);
		
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.CREATED);	
		
	}
	
	
	@PostMapping("/empleados/upload")
	public ResponseEntity<?> upload(@RequestParam("archivo") MultipartFile archivo,@RequestParam("id") Long id){
		
		
		Map<String,Object> response = new HashMap<>();
		Empleado empleado=empleadoService.findById(id);
		
		if(!archivo.isEmpty()) {
			String nombreArchivo=UUID.randomUUID().toString()+"_"+ archivo.getOriginalFilename().replace(" ", "");
			
			Path rutaArchivo = Paths.get("uploads/empleados").resolve(nombreArchivo).toAbsolutePath();
			
			try {
				Files.copy(archivo.getInputStream(), rutaArchivo);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				response.put("message", "Error al subir la imagen: "+nombreArchivo);
				response.put("error", e.getMessage().concat(": ").concat(e.getCause().getMessage()));
				return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);	
			}
			
			String nombreAnterior=empleado.getFoto();
			
			if(nombreAnterior!=null && nombreAnterior.length()>0) {
				Path rutaArchivoAnterior = Paths.get("uploads/empleados").resolve(nombreAnterior).toAbsolutePath();
				//Convierto el path a un tipo archivo
				File archivoFotoAnterior = rutaArchivoAnterior.toFile();
				
				if(archivoFotoAnterior.exists() && archivoFotoAnterior.canRead()) {
					archivoFotoAnterior.delete();
				}
				
			}
			
			
			
			empleado.setFoto(nombreArchivo);
			empleadoService.save(empleado);
			
			response.put("usuario", empleado);
			response.put("mensaje","Imagen subida al sistema correctamente :" + nombreArchivo);
		}
		
		
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.CREATED);	
	}
	
	@GetMapping("/empleados/uploads/img/{nombreFoto:.+}")
	public ResponseEntity<Resource> verFoto(@PathVariable String nombreFoto){
		
		Path rutaArchivo = Paths.get("uploads/empleados").resolve(nombreFoto).toAbsolutePath();
		
		Resource recurso=null;
		try {
			recurso=new UrlResource(rutaArchivo.toUri());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(!recurso.exists() && !recurso.isReadable()) {
			throw new RuntimeException("Error no se pudo cargar la imagen: "+nombreFoto);
		}
		
		HttpHeaders cabecera= new HttpHeaders();
		cabecera.add(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=\""+recurso.getFilename()+"\"");
		
		return new ResponseEntity<Resource>(recurso,cabecera,HttpStatus.OK);
	}
	
	
}
