package com.modulo.springboot.app.item.model.dao;

import org.springframework.data.repository.CrudRepository;


import com.modulo.springboot.app.item.model.entity.Empleado;

public interface IEmpleadoDao extends CrudRepository<Empleado, Long>{
	
	public Empleado findByUsername(String usuario);
	
	public Empleado findByEmail(String email);
}
