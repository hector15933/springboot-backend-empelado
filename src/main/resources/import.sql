INSERT INTO area(authority) VALUES('Administración')
INSERT INTO area(authority) VALUES('Financiera')
INSERT INTO area(authority) VALUES('Compras')
INSERT INTO area(authority) VALUES('Infraestructura')
INSERT INTO area(authority) VALUES('Operación')
INSERT INTO area(authority) VALUES('Talento Humano')
INSERT INTO area(authority) VALUES('Servicios Varios')
INSERT INTO area(authority) VALUES('Etc')

INSERT INTO empleados(primer_nombre,otros_nombres,primer_apellido,segundo_apellido,nacionalidad,tipo_identificacion,enabled,dni,telefono,username,password,fecha_ingreso,create_at,email,area,foto) VALUES('HECTOR','THOMAS ','REYES','CUMPA','Colombia','Cédula de Extranjería',true,'78021291','995707476','user123','$2a$10$DDNB/kjudd5GvexibmBfk.IsI5l6KTmXPLZdx3O7i3Q5v8m7xbPg6',Now(),Now(),'hectorreyes1@cidenet.com.co','Financiera','image.png');
INSERT INTO empleados(primer_nombre,otros_nombres,primer_apellido,segundo_apellido,nacionalidad,tipo_identificacion,enabled,dni,telefono,username,password,fecha_ingreso,create_at,email,area) VALUES('JOSE','LUIS','MARTINEZ','RIMARACHIN','Colombia','Cédula de Extranjería',true,'78021292','995707476','user234','$2a$10$DDNB/kjudd5GvexibmBfk.IsI5l6KTmXPLZdx3O7i3Q5v8m7xbPg6','2020/10/01',Now(),'josemartinez2@cidenet.com.co','Financiera');
INSERT INTO empleados(primer_nombre,otros_nombres,primer_apellido,segundo_apellido,nacionalidad,tipo_identificacion,enabled,dni,telefono,username,password,fecha_ingreso,create_at,email,area) VALUES('BILLY','JOSE','SUAREZ','ROSAS','Estados Unidos','Cédula de Extranjería',true,'78021293','995707476','user254','$2a$10$DDNB/kjudd5GvexibmBfk.IsI5l6KTmXPLZdx3O7i3Q5v8m7xbPg6',Now(),Now(),'billysuarez3@cidenet.com.us','Financiera');
INSERT INTO empleados(primer_nombre,otros_nombres,primer_apellido,segundo_apellido,nacionalidad,tipo_identificacion,enabled,dni,telefono,username,password,fecha_ingreso,create_at,email,area) VALUES('EDGAR','BRAYAN','PERES','TREJO','Colombia','Cédula de Extranjería',true,'78021294','995707476','user789','$2a$10$DDNB/kjudd5GvexibmBfk.IsI5l6KTmXPLZdx3O7i3Q5v8m7xbPg6','2020/10/01',Now(),'edgarperes4@cidenet.com.co','Financiera');


INSERT INTO empleados_areas(empleado_id,areas_id) VALUES(1,1);
INSERT INTO empleados_areas(empleado_id,areas_id) VALUES(2,2);
INSERT INTO empleados_areas(empleado_id,areas_id) VALUES(2,3);
INSERT INTO empleados_areas(empleado_id,areas_id) VALUES(3,3);

	